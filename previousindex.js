const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const bole = require('bole');
const log = require('bole')('kantan');
const garnish = require('garnish');
const devServer = require('./libs/devserver');
const bundler = require('./libs/bundler');
const cssloader = require('./libs/cssloader');
//====== LOGGING SETUP ========
let pretty = garnish({
    level:'info',
    name:'kantan'
})
pretty.pipe(process.stdout);
bole.output({
    level:'info',
    stream:pretty
});


// referencing local modules is a bit different. Without this,
// we might try to pick up modules from where we run Kantan
const LOCAL_MODULE_PATH = `${__filename}/../`;

let BASE_PATH = "";

// do a test to see if we're running this locally or globally - we know bole is necessary so use that as a test.
try {
    require(`${LOCAL_MODULE_PATH}node_modules/bole/bole.js`);
    BASE_PATH = LOCAL_MODULE_PATH;
}catch(e){
    BASE_PATH = `${process.cwd()}/`;
}


module.exports = function(config){
    // ======= START TO SETUP WEBPACK ============ //
    // default entries
    // Note that this takes into account of the newer NPM format of installing all dependencies of dependencies
    // directly within the node_modules folder instead of per dependency

    let hotDevServer = `${BASE_PATH}node_modules/webpack/hot/dev-server`;
    let dev_server = `${BASE_PATH}node_modules/webpack-dev-server/client?http://localhost:${config.port}/`
    let default_entries = [
        hotDevServer,
        dev_server,
        //`./node_modules/webpack/hot/dev-server`,
        //`./node_modules/webpack-dev-server/client?http://localhost:${config.port}/`,
        config.appPath
    ];

    // default plugins
    let default_plugins = [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ];

    // default loaders
    let default_loaders = [
        {
            test:/\.js$/,
            exclude:/node_modules/,
            loader:`${BASE_PATH}node_modules/babel-loader`,
            options: {
                presets: [
                    `${BASE_PATH}node_modules/babel-preset-es2015`
                ]
            }
        }
    ];

    // =========== SETUP CONFIGURATION ========== //
    let configFile = null;
    if(config.customWebpack !== null){
        let configpath = `${config.rootPath}/${config.customWebpack}`
        configFile = require(configpath);

        // build all entry files that need to be built
        let configEntries = configFile.entry || []
        let entries = [...default_entries,...configEntries]
        configFile.entry = entries;

        // append default paths to any new plugins
        let customPlugins = configFile.hasOwnProperty("plugins") !== undefined ? configFile.plugins : [];
        let plugins = [...default_plugins,...customPlugins];
        configFile.plugins = plugins;

        // append append default loaders to custom file
        if(config.buildCSS){
            default_loaders = [...default_loaders,cssloader];
        }

        // it's assumed that the "loaders" or "rules" property is an array.
        let configFileLoaders;
        if(configFile.module.hasOwnProperty("loaders")){
            configFileLoaders = configFile.modules.loaders;
            configFile.module.loaders = [...default_loaders,...configFileLoaders];
        }else if(configFile.module.hasOwnProperty("rules")){
            configFileLoaders = configFile.module.rules;
            configFile.module.rules = [...default_loaders,...configFileLoaders];
        }

    } else {
        // append append default loaders to custom file
        if(config.buildCSS){
            default_loaders = [...default_loaders,cssloader];
        }

        configFile = {
            entry: default_entries,
            performance:{
                hints:false
            },
            output:{
                path:path.resolve(config.output),
                filename:config.appPath.split("/").pop().replace('\.js','\.min\.js'),
                publicPath:"/"
            },
            module:{
                loaders:default_loaders
            },
            plugins: [
                new webpack.HotModuleReplacementPlugin(),
                new webpack.NamedModulesPlugin()
            ]
        }
    }

    // ============ INITIALIZE WEBPACK ================== //
    const compiler = webpack(configFile);

    // ========= INITIALIZE SERVER OR BUILD PROCESS =============== //
    if(config.operationMode === "dev"){
        devServer(compiler,config.devServerConfig);
    } else {
        let buildConfig;


        if(config.buildConfig){
            buildConfig = JSON.parse(fs.readFileSync(options.buildConfig,"utf8"));
        }else{
            buildConfig = {
                src:config.appPath,
                dst:config.buildOutput
            };
        }

        buildConfig["filename"] = config.appPath.split("/").pop().replace('\.js','\.min\.js');

        bundler(buildConfig);
    }
};