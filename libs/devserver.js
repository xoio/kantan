
const WebpackServer = require('webpack-dev-server');
const log = require('bole')('kantan-server');
const defaultConfig = require('./defaultDevServer');

module.exports = function(compiler,serverConfig){
    const root = process.cwd();
    let config = Object.assign(defaultConfig,serverConfig);

    const server = new WebpackServer(compiler,config);

    server.listen(config.port,"localhost",() => {
        log.info("Starting development server at http://localhost:",config.port);
        //open(`http://localhost:${config.port}`);
    });

};