// referencing local modules is a bit different. Without this,
// we might try to pick up modules from where we run Kantan
const LOCAL_MODULE_PATH = `${__filename}/../../`;

let BASE_PATH = "";

// do a test to see if we're running this locally or globally - we know bole is necessary so use that as a test.
try {
    require(`${LOCAL_MODULE_PATH}node_modules/bole/bole.js`);
    BASE_PATH = LOCAL_MODULE_PATH;
}catch(e){
    BASE_PATH = `${process.cwd()}/`;
}


// the default css setup
module.exports = {
    test: /\.css$/,
    use: [
        `${BASE_PATH}node_modules/style-loader`,
        {
            loader: `${BASE_PATH}node_modules/css-loader`
        },
        {
            loader:`${BASE_PATH}node_modules/postcss-loader`,
            options:{
                plugins(){
                    return [
                        require(`${BASE_PATH}node_modules/precss`)
                    ]
                }
            }
        }
    ]
};