const rollup = require('rollup');
const fs = require('fs');
const babel = require('rollup-plugin-babel');
const garnish = require('garnish');
const log = require('bole')('kantan-bundler');
const compile = require('google-closure-compiler-js').compile;

/**
* GLSL plugin for inlining GLSL code.
* @returns {*}
*/
function glsl () {
return {
	transform ( code, id ) {
		if ( !/\.(glsl|frag|vert)$/.test( id ) ) return;
		return 'export default ' + JSON.stringify(
			code
				.replace( /[ \t]*\/\/.*\n/g, '' )
				.replace( /[ \t]*\/\*[\s\S]*?\*\//g, '' )
				.replace( /\n{2,}/g, '\n' )
		) + ';';
	}
};
}

module.exports = function(options){

// ==== setup general properties === //

// compilation level - see closure compiler docs for acceptable values.
options["compilationLevel"] = options["compilationLevel"] !== undefined ? options.compilationLevel : "ADVANCED";

// the language going in to get compiled.
options["languageIn"] = options["languageIn"] !== undefined ? options.languageIn : "ES6";

// the language to output to.
options["languageOut"] = options["languageOut"] !== undefined ? options.languageOut : "ES5";

// this doesn't have anything to do with closure - but you can optionally pass in additional rollup plugins
options["plugins"] = options.plugins !== undefined ? options.plugins : []

// start bundling.
rollup.rollup({
	input:options.src,
	plugins:[
		glsl(),
		babel({
			exclude:'../node_modules/**'
		}),
		...options.plugins
	]
}).then((bundle) => {
	log.info("Beginning final bundle generation.");
	// generate bundle
	bundle.generate({
		format:options.format || "iife",
		name:options.name || "kantan"
	}).then((result) => {
		let compilerOptions = {
			compilationLevel:options.compilationLevel,
			languageIn:options.languageIn,
			languageOut:options.languageOut,
			jsCode:[
				{
					src:result.code
				}
			]
		};

		log.info("Compiling and minifying with Google Closure");

		// compile with closure
		let finalOutput = compile(compilerOptions);
		options["finalOutput"] = finalOutput;

		// write file to filesystem.
		log.info("Writting to filesystem");

		// write file - build output directory if it doesn't exist yet.
		if(!fs.existsSync(options.dst)){
			fs.mkdir(options.dst,() => {
				fs.chmodSync(options.dst,"755");
				writeFile(options);
			});
		} else {
			writeFile(options);
		}

	})


}).catch(err => {
	console.log(err);
});

};

/**
* Write the file to the filesystem
* @param options {Object} the bundler config object you passed into the module.
*/
function writeFile(options){
fs.writeFile(`${options.dst}/${options.filename}`,options.finalOutput.compiledCode,(err)=>{
	if(err){
		log.error("Error writing final bundle");
		throw new Error(err);
	}
	log.info("Bundling complete - can find bundle at ", `${options.dst}/${options.filename}`);
})
}