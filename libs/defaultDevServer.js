const root = process.cwd();
const path = require('path');

module.exports = {
	hot:true,
	noInfo:true,
	contentBase:path.resolve(`${root}/public`),
	publicPath:"/",
	// watch options is necessary on windows.
	watchOptions:{
		poll:1000
	},
	port:9000,
	stats:{
		errors:true,
		hash:false,
		version:false,
		assets:false,
		timings:false,
		chunks:false
	}

}