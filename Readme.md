# Kantan 

A tiny, no-fuss development setup intended for small projects and experiements. It's purpose is to help
ease the beginnings of a project by giving you a little develop setup to get things up and running as fast as possible
without needing to fuss around with remembering all of the common npm packages you might need to work with ES6. 

Once you're done, you can even build with the tool as well. 

# Requirements
* at least node 6.9.1

# Installation
Two ways 
__Method one__
* clone into `node_modules`
* node node_modules/kantan/bin/cli.js

__Method two__
* `npm install -g kantan`
* run like any other command line script

Note that if you use method 2 and a custom webpack config - you may have to install additional packages for your project.

# Use
Kantan is primarily intended for use as a development server, but 
can also be used to build packages(note - building bundles is still under re-write). 

Each form has some available options. You reference all options on the command line or you can 
specify a json or js file to write your config in and pass in a path to that file with the flag `--configFile`

CLI Flags
=== 
* `--build` add this flag if you want to build files instead of running the dev server
* `--port` specifies the port to run the server on 
* `--entry` specifies the path to the main entry file for your app/site
* `--public` specifies the directory that contains your html
* `--customWebpack` Specifies the path to your own webpack.config.js file. Will use a default file if one is not specified.
* `--buildConfig` Specifies the path to a custom build configuration. The build configuration should be a json file. The options you can specify are the same as the 
options for Google's closure compiler which is what is used to minify your Javascript.
* `--buildCSS` By default - css isn't being built to help reduce memory and processing constraints. Add this flag to include the css loader in the compiled webpack config. You can then 
include css like you normally would with style-loader/css-loader by going `include css from '<path to your css file>`

Building 
===========
Previously, there was the option to build a bundle in a common js compatible format. 
Now that ES6 is starting to be directly integrated into browsers, it's assumed that ES6 is what you're going to be building with and 
common js support is removed. 

There are some sensible defaults set up now but if need be you can pass in a custom json file specifying build options. 
See `--buildConfig` flag above.

CSS Parsing 
===== 
This dev setup is primarily meant to JS based projects, but CSS parsing is included as well.
This is utilizing [PostCSS](https://github.com/postcss/postcss) as a pre-processing step as it has a large variety of plugins to change your css
however you'd like. By default, it'll compile scss syntax using [PreCss](https://github.com/jonathantneal/precss). From previous testing, there isn't a
difference that I can find yet, but keep in mind that this isn't using the normal Sass rubygem.

Note that this no longer uses chokidar and is all tied into Webpack.
