module.exports =  [
    {
        test:/\.(glsl|vert|frag|vs|fs)$/,
        use:'raw-loader'
    },
    {
        test:/\.js$/,
        exclude:/(node_modules | bower_components)/,
        use:{
            loader:'babel-loader',
            options:{
                presets:[
                    "env"
                ],
                plugins:[
                    "syntax-dynamic-import"
                ]
            }
        }
    }
]