#!/usr/bin/env node
const path = require('path');
const fs = require('fs');
let argv = require('minimist')(process.argv.slice(2));
const webpack = require('webpack');
const default_loaders = require('./loaders');
const default_plugins = require('./plugins')

/**
 *
 * CLI SETUP
 *
 * Prepares everything for Kantan to process based on user input.
 *
 * The big change from previous versions is that a lot of the default
 * setup will be found here now.
 */



    // =============== PRE-INIT ======================= //

const mode = argv.mode !== undefined ? argv.mode : "development";


// the root will always be wherever the script is run.
const ROOT = process.cwd();

// signal to indicate whether or not we have a config file.
let haveConfig = false;

// if we're using a JSON based or .js configuration file, build argv file from that instead.
if(argv.configFile){
    let custom;
    if(argv.configFile.search(".json") !== -1){
        custom = JSON.parse(fs.readFileSync(`${ROOT}/${argv.configFile}`));
    }else if(argv.configFile.search(".js") !== -1){
        custom = require(`${ROOT}/${argv.configFile}`);
    }
    Object.assign(argv,custom);

    haveConfig = true;
}


// =================== SETUP CORE VARIABLES ================ //
// build out core variables
const PORT = argv.port || 9000;


// referencing local modules is a bit different. Without this,
// we might try to pick up modules from where we run Kantan
const LOCAL_MODULE_PATH = `${__filename}/../`;

let BASE_PATH = "";

// do a test to see if we're running this locally or globally - we know bole is necessary so use that as a test.
try {
    require(`${LOCAL_MODULE_PATH}node_modules/bole/bole.js`);
    BASE_PATH = LOCAL_MODULE_PATH;
}catch(e){
    BASE_PATH = `${process.cwd()}/`;
}

let hotDevServer = `${BASE_PATH}node_modules/webpack/hot/dev-server`;
let dev_server = `${BASE_PATH}node_modules/webpack-dev-server/client?http://localhost:${PORT}/`


// ============ FIGURE OUT WEBPACK ENTRIES ================ //

let entries = argv.entry !== undefined ? argv.entry : "./src/app.js";

let finalEntries = null;

if(mode !== "production"){
    finalEntries =  [
        hotDevServer,
        dev_server,
        entries
    ];

}else if(mode === "production"){
    finalEntries = [
        entries
    ]
}



// ============ FIGURE OUT WEBPACK PLUGINS================ //

// I don't believe it's possible to somehow stringify in plugin code so we only do this if we have a
// config file of some kind
let finalPlugins = [];
if(mode !== "production"){
    if(haveConfig && argv.hasOwnProperty("plugins")){
        finalPlugins = [
            ...default_plugins,
            ...argv.plugins
        ]
    }else{
        finalPlugins = default_plugins;
    }

}else{
    finalPlugins = argv.plugins;
}



// ============ FIGURE OUT WEBPACK LOADERS ================ //
let finalLoaders = []
if(mode !== "production"){
    if(haveConfig && argv.hasOwnProperty("loaders")){
        finalLoaders = [
            ...default_loaders,
            ...argv.loaders
        ]
    }else {
        finalLoaders = default_loaders
    }
}else{
    finalLoaders = argv.loaders;
}
// ================= FIGURE OUTPUT POINTS ============== //
let outputPath = argv.outPath !== undefined ? argv.outPath : path.resolve(ROOT,"dist");
let outputFilename = argv.outFile !== undefined ? argv.outName : "app.js";

// ================= BUILD FINAL OPTIONS OBJECT ============== //


// note - matches keys of what's expected now
let options = {

    mode:mode,

    // entries
    entry:finalEntries,

    // plugins
    plugins:finalPlugins,

    // loader rules
    loaders:finalLoaders,

    // file output options
    path:outputPath,
    filename:outputFilename,

    // server rules
    // see https://webpack.js.org/configuration/dev-server/#devserver-compress
    // for complete rules
    devServer:{
        port:PORT,
        compress:true,
        historyApiFallback:true,
        hot:true,
        open:true
    }
}


// if we've declared a custom webpack file, use that instead by overwriting default options
if(argv.hasOwnProperty("customWebpack")){
    options["customWebpack"] = require(`${ROOT}/${argv.customWebpack}`);

    if(mode !== "production"){
        // rebuild entry key so we can include reloading
        options["customWebpack"].entry = [
            hotDevServer,
            dev_server,
            options["customWebpack"].entry
        ]

        // push necessary plugins
        options["customWebpack"].plugins = [
            ...default_plugins,
            ...options["customWebpack"].plugins

        ]
    }
}


require('../index.js')(options);