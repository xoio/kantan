const webpack = require('webpack')

/**
 * Exports default plugins we want to use. 
 */
module.exports =  [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
]