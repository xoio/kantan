const webpack = require('webpack');
const server = require('./libs/devserver');
const bole = require('bole');
const log = require('bole')('kantan');
const garnish = require('garnish');
module.exports = function(options){


    //====== LOGGING SETUP ========
    let pretty = garnish({
        level:'info',
        name:'kantan'
    })
    pretty.pipe(process.stdout);
    bole.output({
        level:'info',
        stream:pretty
    });




    // ======= BUILD CONFIG =========== //
    // if using own webpack file, that is what the options param becomes.
    // if not, build a default one based on options
    let config = options.customWebpack !== undefined ? options.customWebpack : {
        entry:options.entry,
        mode:options.mode,
        output:{
            path:options.path,
            filename:options.filename,
            globalObject:"this"
        },
        plugins:options.plugins,
        module:{
            rules:options.loaders
        }
    };


    // =========== INIT WEBPACK ============ //
    const compiler = webpack(config);

    if(options.mode === "development"){
        server(compiler,options.devServer);
    }else if(options.mode === "production"){

        config.mode = "production";

        webpack(config,(err,status) => {
            if(err){
                console.log(err);
            }
        });
    }
};




